﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using GoogleMobileAds;
using System;

public class AdMobScript : MonoBehaviour
{

    private InterstitialAd interstitial;
    private RewardBasedVideoAd reward;
    public Text rewardText;

    void Start()
    {
        MobileAds.Initialize("ca-app-pub-7104477320662865~2878420992");

        this.reward = RewardBasedVideoAd.Instance;
        this.reward.OnAdLoaded += HandleRewardBasedVideoLoaded;
        this.reward.OnAdRewarded += HandleRewardBasedVideoRewarded;
    }

    void Update()
    {
        
    }
    public void OnClickRewardInstance()
    {
        this.RequestRewardBasedVideo();
    }
    public void OnClickInterstitial()
    {
        RequestInterstitial();

        //Lancio un evento su firebased.
        Firebase.Analytics.FirebaseAnalytics.LogEvent(
          Firebase.Analytics.FirebaseAnalytics.EventSelectContent,
          new Firebase.Analytics.Parameter[] {
              new Firebase.Analytics.Parameter(
                Firebase.Analytics.FirebaseAnalytics.ParameterItemId, 1),
              new Firebase.Analytics.Parameter(
                Firebase.Analytics.FirebaseAnalytics.ParameterItemName, "name"),
              new Firebase.Analytics.Parameter(
                Firebase.Analytics.FirebaseAnalytics.UserPropertySignUpMethod, "Google"),
              new Firebase.Analytics.Parameter(
                "click", "click on interstitial"),
              new Firebase.Analytics.Parameter(
                "user_id", "Filippo"),
          }
        );
    }

    private void RequestInterstitial()
    {
        #if UNITY_ANDROID
                string adUnitID = "ca-app-pub-3940256099942544/1033173712";
        #elif UNITY_IPHONE
                        string adUnitID = "ca-app-pub-7104477320662865/6270871099";
        #else
                        string adUnitID = "unexpected_platform";
        #endif

        if (this.interstitial != null)
            this.interstitial.Destroy();

        this.interstitial = new InterstitialAd(adUnitID);
        this.interstitial.OnAdLoaded += HandleOnAdLoaded;

        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);

        Debug.Log("Request Interstitial");

    }
   

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
        }
    }

    private void RequestRewardBasedVideo()
    {
        #if UNITY_ANDROID
                string adUnitId = "ca-app-pub-3940256099942544/5224354917";
        #elif UNITY_IPHONE
                    string adUnitId = "ca-app-pub-3940256099942544/1712485313";
        #else
                    string adUnitId = "unexpected_platform";
        #endif

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.reward.LoadAd(request, adUnitId);
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        rewardText.text = 
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type;
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        if (this.reward.IsLoaded())
        {
            this.reward.Show();
        }
    }
}
