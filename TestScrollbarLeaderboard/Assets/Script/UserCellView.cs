﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class UserCellView : EnhancedScrollerCellView
{
    public Text UserId;
    public Text MaxScore;
    public Text TotalScore;
    public Text Date;

    public void SetData(ScrollerData data)
    {
        this.UserId.text = data.UserId;
        this.MaxScore.text = data.MaxS+"";
        this.TotalScore.text = data.TotalS+"";
        this.Date.text = data.Date;
    }
}
