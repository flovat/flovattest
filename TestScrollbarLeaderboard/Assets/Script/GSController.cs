﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using System;

//Questa classe ha il compito di gestire il login e tutte le comunicazioni server client.

public class GSController : MonoBehaviour
{
    //Login Panel
    public GameObject PanelLogin;
    public InputField UserInput;
    public InputField UserPassword;
    public Text ToastLogin;

    //Data Panel
    public GameObject PanelData;
    public InputField NicknameField;
    public InputField ScoreField;
    public Text ToastData;

    //Gestione dei dati leaderboard
    private List<ScrollerData> _LeaderboardData;

    void Start()
    {
        PanelData.SetActive(false);
        //Inizializzo la struttura dati per memorizzare i valori della leaderboard.
        _LeaderboardData = new List<ScrollerData>();
    }

    void Update()
    {
        
    }

    /*Gestione del login da parte dell'utente.*/
    public void OnClickButtonLogin(){   StartCoroutine(functionLogIn());    }
    //Coroutine per la registrazione e l'autenticaizone.
    //Dopo la regisrazione metto in wait per un secondo prima di autenticarmi.
    private IEnumerator functionLogIn()
    {
        if (UserInput.text != "" && UserPassword.text != "")
        {
            while (true)
            {
                ToastLogin.text = "Loading...";

                new RegistrationRequest()
                .SetDisplayName(UserInput.text)
                .SetPassword(UserPassword.text)
                .SetUserName(UserInput.text)
                .Send((response) =>
                {
                    ToastLogin.text = (response.HasErrors) ? "Checking" : "Registration Done";
                    ToastLogin.color = (response.HasErrors) ? Color.red : Color.green;
                });

                yield return new WaitForSeconds(2f);

                new AuthenticationRequest().SetUserName(UserInput.text).SetPassword(UserPassword.text).Send((response) =>
                {
                    ToastLogin.text = (response.HasErrors) ? "Autentication Failed" : "Autentication Done";
                    ToastLogin.color = (response.HasErrors) ? Color.red : Color.green;
                    PanelData.SetActive(!response.HasErrors);
                });

                break;
            }
        }
        else
        {
            ToastLogin.text = "Empty fields. Please retry.";
            ToastLogin.color = Color.red;
        }
    }


    /*Gestione dell'invio dei dati all'evento per popolare la leaderboard.*/
    public void OnClickEvent()
    {
        //Controllo che i campi non siano vuoti.
        //Migliorare il controllo per verificare l'invio di un intero e di una stringa neii giusti campi.
        if (NicknameField.text != "" && ScoreField.text != "")
        {
            new LogEventRequest()
            .SetEventKey("Lovat_Leaderboard_2")
            .SetEventAttribute("UserId", NicknameField.text)
            .SetEventAttribute("UserScore", Int32.Parse(ScoreField.text))
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    //Gestisco la risposta del server GS.
                    ToastData.text = "Request Done.";
                    ToastData.color = Color.green;
                }
                else
                {

                    ToastData.text = "Request Failed.";
                    ToastData.color = Color.red;
                }

            });
        }
        else
        {
            ToastData.text = "Input Error. Please retry.";
            ToastData.color = Color.red;
        }
    }

    /*Gestione del bottone per il refresh della scrollview*/
    public void OnClickReloadButton()
    {
        new GameSparks.Api.Requests.LeaderboardDataRequest().SetLeaderboardShortCode("Lovat_Leaderboard_Board").SetEntryCount(10).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Dati leaderboard trovati.");
                //Inizializzo nuovamente la lista.
                _LeaderboardData = new List<ScrollerData>();
                foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data)
                {
                    //Debug.Log("Date completed : "+entry.JSONString);
                    string totalScore = entry.JSONData["SUM-UserTotalScore"].ToString();
                    string nickname = entry.UserName;
                    string data = entry.JSONData["LAST-Date"].ToString();
                    string maxScore = entry.JSONData["MAX-UserScore"].ToString();
                    
                    ScrollerData newData = new ScrollerData(nickname, maxScore, totalScore, data);
                    _LeaderboardData.Add(newData);
                }

                ScrollerController.getSingleton().reloadData(this._LeaderboardData);
            }
            else
            {
                Debug.Log("Error Retrieving Leaderboard Data...");
            }
        });
    }
}
