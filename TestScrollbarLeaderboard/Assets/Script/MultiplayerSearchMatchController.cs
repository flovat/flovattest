﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using System;

public class MultiplayerSearchMatchController : MonoBehaviour
{

    public Text ToastMatchmaking;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    /*Gestione del login da parte dell'utente.*/
    public void OnClickButtonSearchMatch() { StartCoroutine(functionMatchmaking()); }
    //Coroutine per la registrazione e l'autenticaizone.
    //Dopo la regisrazione metto in wait per un secondo prima di autenticarmi.
    private IEnumerator functionMatchmaking()
    {
        Debug.Log("Search match request.");
        new MatchmakingRequest()
        .SetMatchShortCode("FLT")
        .SetSkill(2)
        .Send((response) => {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                ToastMatchmaking.color = Color.green;
                ToastMatchmaking.text = response.JSONData.ToString();
                Debug.Log("Response matchmaking: "+response.JSONString);
            }
            else
            {
                ToastMatchmaking.text = "Error.";
                ToastMatchmaking.color = Color.red;
            }
        });
        
        yield return new WaitForSeconds(10f);
    }
}
