﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollerData 
{
    public string UserId;
    public string MaxS;
    public string TotalS;
    public string Date;

    public ScrollerData(string nickname, string maxscore, string totalscore, string date)
    {
        this.UserId = nickname;
        this.MaxS = maxscore;
        this.TotalS = totalscore;
        this.Date = date;
    }
}
