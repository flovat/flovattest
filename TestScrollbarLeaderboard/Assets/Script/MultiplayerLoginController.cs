﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using System;

public class MultiplayerLoginController : MonoBehaviour
{
    //Login Panel
    public GameObject PanelLogin;
    public InputField UserInput;
    public InputField UserPassword;
    public Text ToastLogin;
    public GameObject PanelSearchMatch;

    // Start is called before the first frame update
    void Start()
    {
        PanelSearchMatch.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*Gestione del login da parte dell'utente.*/
    public void OnClickButtonLogin() { StartCoroutine(functionLogIn()); }
    //Coroutine per la registrazione e l'autenticaizone.
    //Dopo la regisrazione metto in wait per un secondo prima di autenticarmi.
    private IEnumerator functionLogIn()
    {
        if (UserInput.text != "" && UserPassword.text != "")
        {
            while (true)
            {
                ToastLogin.text = "Loading...";

                new RegistrationRequest()
                .SetDisplayName(UserInput.text)
                .SetPassword(UserPassword.text)
                .SetUserName(UserInput.text)
                .Send((response) =>
                {
                    ToastLogin.text = (response.HasErrors) ? "Checking" : "Registration Done";
                    ToastLogin.color = (response.HasErrors) ? Color.red : Color.green;
                });

                yield return new WaitForSeconds(2f);

                new AuthenticationRequest().SetUserName(UserInput.text).SetPassword(UserPassword.text).Send((response) =>
                {
                    ToastLogin.text = (response.HasErrors) ? "Autentication Failed" : "Autentication Done";
                    ToastLogin.color = (response.HasErrors) ? Color.red : Color.green;
                    PanelSearchMatch.SetActive(!response.HasErrors);
                });

                break;
            }
        }
        else
        {
            ToastLogin.text = "Empty fields. Please retry.";
            ToastLogin.color = Color.red;
        }
    }
}
