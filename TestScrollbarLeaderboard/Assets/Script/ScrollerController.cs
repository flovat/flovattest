﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;

public class ScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{

    private List<ScrollerData> _data;

    public EnhancedScroller myScroller;
    public UserCellView myCellPrefab;

    public static ScrollerController singleton = null;

    void Awake()
    {
        if (singleton == null) singleton = this;
    }

    public static ScrollerController getSingleton() { return singleton; }

    void Start()
    {


        
    }

    void Update()
    {
        
    }

    //Metodo richiamato dalla classe GSController quando viene aggiornata la vista della scrollview.
    public void reloadData(List<ScrollerData> listData)
    {
        this._data = listData;
        myScroller.Delegate = this;
        myScroller.ReloadData();
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 100f;
    }
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        UserCellView cellView = scroller.GetCellView(myCellPrefab) as UserCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }

}
